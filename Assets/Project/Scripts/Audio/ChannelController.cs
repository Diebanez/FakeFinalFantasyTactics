﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ChannelController : MonoBehaviour {

    public AudioSource Source;
    public event Action Play;
    public event Action Stop;
    public event Action Pause;
    public event Action Resume;

    bool IsPaused = false;

    void Awake()
    {
        Source = gameObject.AddComponent<AudioSource>();
    }

    void Update()
    {
        if(gameObject.activeInHierarchy && !Source.isPlaying && !IsPaused)
        {
            if (Stop != null)
            {
                Stop();
            }
        }
    }

    public void StopClip()
    {
        if (Source.isPlaying)
        {
            Source.Stop();
        }
    }

    public void ResumeClip()
    {
        if (!Source.isPlaying)
        {
            Source.Play();
        }
        if (Resume != null)
        {
            Resume();
        }
    }

    public void PauseClip()
    {
        if (Source.isPlaying)
        {
            Source.Pause();
        }
        IsPaused = true;
        if (Pause != null)
        {
            Pause();
        }
    }
}
