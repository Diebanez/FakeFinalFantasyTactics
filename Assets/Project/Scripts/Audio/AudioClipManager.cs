﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipManager : Singleton<AudioClipManager>
{
    public Dictionary<string, AudioClip> Clips = new Dictionary<string, AudioClip>();

    public void Load()
    {
        foreach (AudioClip clip in Resources.LoadAll<AudioClip>("Audio/Clips"))
        {
            Clips.Add(clip.name, clip);
        }
        //InputManager.instance.TileClicked += this.OnObjectClick;
        //InputManager.instance.PlayerClicked += this.OnObjectClick;
        InputManager.instance.MovementSelected += this.OnActionClick;
        InputManager.instance.AttackSelected += this.OnActionClick;
        TurnManager.instance.NewTurn += this.OnNewTurnClick;
        BoardManager.instance.BoardGenerated += OnBoardGenerated;
    }

    void OnBoardGenerated()
    {
        foreach (PlayerController pc in PlayerManager.instance.Players)
        {
            pc.AttackExecuted += OnAttack;
            pc.MoveStart += OnActionClick;
        }
    }

    void OnAttack(PlayerController ActualPlayer, PlayerController TargetPlayer)
    {
        AudioManager.instance.Play(Clips["Shoot"], false, 1);
    }

    void OnObjectClick(GameObject ObjectClcked)
    {
        AudioManager.instance.Play(Clips["ActionClick"], false, 1);
    }

    void OnNewTurnClick(PlayerController ActualPlayer)
    {
        AudioManager.instance.Play(Clips["NewTurn"], false, 1);
    }

    void OnActionClick(PlayerController ActualPlayer)
    {
        AudioManager.instance.Play(Clips["ActionClick"], false, 1);
    }
}
