﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyAudioSource : IPoolable {

    public ChannelController Channel { get; set; }
    public event Action<object> Deactivate;

    public MyAudioSource()
    {
        GameObject MyObject = new GameObject("AudioSource");
        MyObject.transform.parent = AudioManager.instance.transform;
        Channel = MyObject.AddComponent<ChannelController>();
        Channel.Stop += this.StopClip;
    }

    public void SetActivation(bool value)
    {
        Channel.gameObject.SetActive(value);
    }

    public void StopClip()
    {
        AudioManager.instance.ActiveChannels.Remove(this);
        this.Deactivate(this);
    }

    public void ResumeClip()
    {
        Channel.ResumeClip();
    }

    public void PauseClip()
    {
        Channel.PauseClip();
    }
}
