﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
	[SerializeField]
	static int ChannelNumber = 10;
    ObjectPool<MyAudioSource> MySources;
    public List<MyAudioSource> ActiveChannels = new List<MyAudioSource>();

	void Start ()
	{
        GeneratePool();
	}

    void GeneratePool()
    {
        if(MySources != null)
        {
            return;
        }
        MySources = new ObjectPool<MyAudioSource>(ChannelNumber);
        for (int i = 0; i < ChannelNumber; i++)
        {
            MySources.Fetch(new MyAudioSource());
        }
    }

    public void Play(AudioClip Clip, bool Loop, float Volume)
    {
        GeneratePool();
        MyAudioSource Source = MySources.Pull();
        if(Source == null)
        {
            Debug.Log("No more channels available");
        }
        else
        {
            Source.Channel.Source.clip = Clip;
            Source.Channel.Source.loop = Loop;
            Source.Channel.Source.volume = Volume;
            Source.Channel.Source.Play();
            ActiveChannels.Add(Source);
        }
    }

    public void PlayAt(AudioClip Clip, bool Loop, float Volume, Vector3 Position)
    {
        MyAudioSource Source = MySources.Pull();
        if (Source == null)
        {
            Debug.Log("No more channels available");
        }
        else
        {
            Source.Channel.Source.clip = Clip;
            Source.Channel.Source.loop = Loop;
            Source.Channel.Source.volume = Volume;
            Source.Channel.Source.gameObject.transform.position = Position;
            Source.Channel.Source.Play();
            ActiveChannels.Add(Source);
        }
    }

    public void Resume(AudioClip Clip)
    {
        if(ActiveChannels.Count > 0)
        {
            foreach(MyAudioSource Channel in ActiveChannels)
            {
                if(Channel.Channel.Source.clip == Clip)
                {
                    Channel.ResumeClip();
                }
            }
        }
        else
        {
            Debug.Log("Clip is not Playing");
        }
    }

    public void Pause(AudioClip Clip)
    {
        if (ActiveChannels.Count > 0)
        {
            foreach (MyAudioSource Channel in ActiveChannels)
            {
                if (Channel.Channel.Source.clip == Clip)
                {
                    Channel.PauseClip();
                }
            }
        }
        else
        {
            Debug.Log("Clip is not Playing");
        }
    }

    public void Stop(AudioClip Clip)
    {
        if (ActiveChannels.Count > 0)
        {
            Queue<MyAudioSource> MyQueue = new Queue<MyAudioSource>();
            foreach (MyAudioSource Channel in ActiveChannels)
            {
               
                if (Channel.Channel.Source.clip == Clip)
                {
                    MyQueue.Enqueue(Channel);
                }
            }
            while(MyQueue.Count > 0)
            {
                MyQueue.Dequeue().StopClip();
            }
        }
        else
        {
            Debug.Log("Clip is not Playing");
        }
    }
}
