﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/BoardSettings", fileName = "BoardSettings")]
public class BoardSettings : SingletonScriptableObject<BoardSettings> {
    public BoardType TypeOfBoard;
    public int BoardWidth;
    public Material[] LevelMaterials;
    public GameObject TilePrefab;
    public GameObject UnreachableTilePrefab;
    public float HeightScale;
    
}
