﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Settings/DataManager", fileName = "DataManager")]
public class DataManager : SingletonScriptableObject<DataManager> {

    public List<PlayerData> PlayerDatas;

    private void Awake()
    {
        if(PlayerDatas == null)
        {
            PlayerDatas = new List<PlayerData>();
        }
    }
    public void SavePlayerData(PlayerData Data)
    {
        int SearchData = -1;
        for(int i = 0; i < PlayerDatas.Count; i++)
        {
            if (PlayerDatas[i].Name == Data.Name)
            {
                SearchData = i;
                break;
            }
        }
        if (SearchData >= 0)
        {
            PlayerDatas[SearchData].UpdateDatas(Data);
        }
        else
        {
            PlayerDatas.Add(Data);
        }

    }

    public void OnValidate()
    {
        foreach(PlayerData PD in PlayerDatas)
        {
            PD.Class = PD.InspectorClassValue;
            PD.Health = PD.InspectorHealth;
        }
    }
}
