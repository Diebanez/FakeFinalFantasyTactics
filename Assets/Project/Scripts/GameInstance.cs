﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInstance : Singleton<GameInstance> {

    public void LoadNewGame()
    {
        AudioClipManager.instance.Load();
        BoardManager.instance.GenerateRandomBoard();
        PlayerManager.instance.TeamDied += OnTeamDied;
    }

    void OnTeamDied(int VictoryTeam)
    {
        SaveMatchDatas();
        if (VictoryTeam < 0)
        {
            Debug.Log("TeamOneWin");
            StartCoroutine(ViewEndGameScreen(true));
        }
        else
        {
            Debug.Log("TeamTwoWin");
            StartCoroutine(ViewEndGameScreen(false));
        }        
        //GameManager.instance.LoadShop();
    }

    public void SaveMatchDatas()
    {
        foreach(PlayerController PC in PlayerManager.instance.Players)
        {
            DataManager.instance.SavePlayerData(PC.MyDatas);
        }
    }

    IEnumerator ViewEndGameScreen(bool Win)
    {
        yield return new WaitForSeconds(2);
        if (Win)
        {
            GameManager.instance.LoadWinScreen();
        }
        else
        {
            GameManager.instance.LoadLoseScreen();
        }
    }
}
