﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LightSaberVFX : MonoBehaviour {

    [SerializeField]
    Transform StartPosition;

    [SerializeField]
    Transform EndPosition;
    
    LineRenderer BladeRenderer;
    private float TextureOffset = 0.0f;

    private void Start()
    {
        BladeRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        BladeRenderer.SetPosition(0, StartPosition.position);
        BladeRenderer.SetPosition(1, EndPosition.position);

        TextureOffset -= Time.deltaTime * 2f;
        if(TextureOffset < -10f)
        {
            TextureOffset += 10f;
        }
        BladeRenderer.sharedMaterials[1].SetTextureOffset("_MainTex", new Vector2(TextureOffset, 0.0f));
    }
}
