﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{
    public Vector3 StartPosition;
    public Vector3 TargetPosition;
    public float TotalTime = .1f;

    float TimePassed = 0.0f;

    private void Update()
    {
        if(StartPosition != null && TargetPosition != null)
        {
            transform.position = Vector3.Lerp(StartPosition, TargetPosition, TimePassed / TotalTime);
            TimePassed += Time.deltaTime;
        }
    }
}
