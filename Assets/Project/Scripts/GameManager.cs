﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : PersistentSingleton<GameManager> {

    public GameObject LoadingScreenPrefab;
    GameObject LoadingScreenInstance;
    public Slider LoadSlider;

    private void Start()
    {
        LoadingScreenInstance = Instantiate(LoadingScreenPrefab);
        LoadingScreenInstance.transform.parent = this.transform;
        LoadSlider = LoadingScreenInstance.GetComponentInChildren<Slider>();
        LoadingScreenInstance.SetActive(false);
        SceneManager.sceneLoaded += this.OnSceneLoaded;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            LoadBattle();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            LoadShop();
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            LoadMenu();
        }
    }

    public void LoadBattle()
    {
        SceneManager.LoadScene("scn_Battle");
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("scn_MainMenu");
    }

    public void LoadShop()
    {
        SceneManager.LoadScene("scn_Shop");
    }
    public void LoadLoseScreen()
    {
        SceneManager.LoadScene("scn_LoseScreen");
    }
    public void LoadWinScreen()
    {
        SceneManager.LoadScene("scn_WinScreen");
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {        
        if (scene.name == "scn_Battle")
        {
            LoadingScreenInstance.SetActive(true);
            LoadSlider.value = 0.0f;
            GameInstance.instance.LoadNewGame();
        }
        else
        {
            LoadingScreenInstance.SetActive(false);
        }
    }

    public void OnBoardGenerated()
    {
        LoadingScreenInstance.SetActive(false);
    }
}
