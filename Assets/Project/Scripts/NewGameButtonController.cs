﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGameButtonController : MonoBehaviour {
    public void NewGame()
    {
        GameManager.instance.LoadBattle();
    }
}
