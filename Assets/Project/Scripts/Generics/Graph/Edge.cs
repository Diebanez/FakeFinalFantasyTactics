﻿using System;
using System.Collections.Generic;

public class Edge<T> where T : IComparable {
    public Node<T> StartNode { get; private set; }
    public Node<T> EndNode { get; private set; }
    public int Width { get; private set; }

    public Edge(Node<T> StartNode, Node<T> EndNode, int Width)
    {
        this.StartNode = StartNode;
        this.EndNode = EndNode;
        this.Width = Width;
    }
}
