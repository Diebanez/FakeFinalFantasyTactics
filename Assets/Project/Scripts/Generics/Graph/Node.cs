﻿using System;

public class Node<T> where T : IComparable {
    public T Item { get; private set; }

    public Node(T Item)
    {
        this.Item = Item;
    }
}
