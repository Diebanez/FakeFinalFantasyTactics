﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Graph<T> where T : IComparable
{
    public List<Node<T>> Nodes { get; set; }
    public List<Edge<T>> Edges { get; set; }

    public Dictionary<T, Node<T>> NodesDictionary { get; private set; }
    public Dictionary<Node<T>, List<Edge<T>>> EdgesDictionary { get; private set; }

    //Dijkstra variables
    Dictionary<T, T> PreviousNodes;
    Dictionary<T, int> Distances;

    T DijkstraSource = default(T);

    public Graph(){
        Nodes = new List<Node<T>>();
        Edges = new List<Edge<T>>();
        NodesDictionary = new Dictionary<T, Node<T>>();
        EdgesDictionary = new Dictionary<Node<T>, List<Edge<T>>>();
    }

    public void AddNode(T Item)
    {
        if (GetNode(Item) == null)
        {
            Nodes.Add(new Node<T>(Item));
            NodesDictionary.Add(Item, Nodes.Last());
        }
    }

    public void AddEdge(T StartItem, T EndItem, int Width)
    {
        if(NodesDictionary.ContainsKey(StartItem) && NodesDictionary.ContainsKey(EndItem))
        {
            if (EdgesDictionary.ContainsKey(NodesDictionary[StartItem]))
            {
                foreach(Edge<T> ed in EdgesDictionary[NodesDictionary[StartItem]])
                {
                    if(ed.EndNode.Item.CompareTo(EndItem) == 0)
                    {
                        //throw (new Exception("Already exist this connection"));
                        return;
                    }
                }
                if (StartItem == null || NodesDictionary[StartItem] == null)
                {
                    //throw new Exception("undefined startnode");
                    return;
                }
                Edges.Add(new Edge<T>(NodesDictionary[StartItem], NodesDictionary[EndItem], Width));
                EdgesDictionary[NodesDictionary[StartItem]].Add(Edges.Last());
            }
            else
            {
                EdgesDictionary.Add(NodesDictionary[StartItem], new List<Edge<T>>());
                if (StartItem == null || NodesDictionary[StartItem] == null)
                {
                    //throw new Exception("undefined startnode");
                    return;
                }
                Edges.Add(new Edge<T>(NodesDictionary[StartItem], NodesDictionary[EndItem], Width));
                EdgesDictionary[NodesDictionary[StartItem]].Add(Edges.Last());
               
            }
        }
        else
        {
            //throw new Exception("The Graph doesn't have this node");
            return;
        }
    }

    public Node<T> GetNode(T Item)
    {
        if (NodesDictionary.ContainsKey(Item))
        {
            return NodesDictionary[Item];
        }
        else
        {
            foreach (Node<T> n in Nodes)
            {
                if (n.Item.CompareTo(Item) == 0)
                {
                    return n;
                }
            }
            return null;
        }
    }

    public void SetDijkstraSource(T Source)
    {
        Node<T> MySource = GetNode(Source);
        if (MySource == null)
        {
            //throw new Exception("This Node doesn't belong to the graph");
            return;
        }

        DijkstraSource = MySource.Item;

        Distances = new Dictionary<T, int>();
        PreviousNodes = new Dictionary<T, T>();

        foreach(Node<T> m_Node in Nodes)
        {
            Distances.Add(m_Node.Item, int.MaxValue);
            PreviousNodes.Add(m_Node.Item, default(T));
        }
        if (!Distances.ContainsKey(MySource.Item))
        {
            throw new Exception("Source Key not founded");
        }
        Distances[MySource.Item] = 0;

        List<T> NodesQueue = new List<T>();        

        foreach(Node<T> m_Node in Nodes)
        {
            NodesQueue.Add(m_Node.Item);
        }

        T Nearest;

        while(NodesQueue.Count > 0)
        {
            NodesQueue.Sort((x, y) => Distances[x] - Distances[y]);
            Nearest = NodesQueue[0];
            NodesQueue.RemoveAt(0);

            if(Distances[Nearest] == int.MaxValue)
            {
                break;
            }
            if (NodesDictionary.ContainsKey(Nearest) && EdgesDictionary.ContainsKey(NodesDictionary[Nearest]))
            {
                foreach (Edge<T> Neighbour in EdgesDictionary[NodesDictionary[Nearest]])
                {
                    int AlternativeDistance = Distances[Nearest] + Neighbour.Width;
                    if (AlternativeDistance < Distances[Neighbour.EndNode.Item])
                    {
                        Distances[Neighbour.EndNode.Item] = AlternativeDistance;
                        PreviousNodes[Neighbour.EndNode.Item] = Nearest;
                    }
                }
            }
        }
    }

    public List<T> GetShortestPathTo(T Target)
    {
        if(Distances == null || PreviousNodes == null)
        {
            return null;
        }
        List<T> Sequence = new List<T>();

        T NodeChecker = Target;

        Sequence.Add(GetNode(NodeChecker).Item);
        while(PreviousNodes[GetNode(NodeChecker).Item] != null)
        {
            Sequence.Add(PreviousNodes[GetNode(NodeChecker).Item]);
            NodeChecker = PreviousNodes[GetNode(NodeChecker).Item];
        }

        List<T> Ret = new List<T>();

        for(int i = Sequence.Count - 1; i >= 0; i--)
        {
            Ret.Add(Sequence[i]);
        }
        if(Ret[0].CompareTo(DijkstraSource)!= 0)
        {
            Ret = null;
        }
        return Ret;
    }

}

