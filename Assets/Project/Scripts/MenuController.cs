﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public void NewGame()
    {
        GameManager.instance.LoadBattle();
    }

    public void LoadShop()
    {
        GameManager.instance.LoadShop();
    }
}