﻿using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoardTile : IComparable {
    public float XPos { get; set; }
    public float YPos { get; set; }
    public float Height { get; set; }
    public bool IsReachable = true;

    public BoardTile(float XPos, float YPos, float Height)
    {
        this.XPos = XPos;
        this.YPos = YPos;
        this.Height = Height;
    }

    public int CompareTo(object obj)
    {
        try
        {
            var other = (BoardTile)obj;
            if (XPos == other.XPos && YPos == other.YPos)
            {
                return 0;
            }
            return 1;
        }catch(Exception ex)
        {
            throw new Exception(ex.ToString());
        }
    }

    public Vector3 ToPlayerVector3(BoardType m_TypeOfBoard, float m_HeightScale)
    {
        switch (m_TypeOfBoard)  
        {
            case BoardType.Squared:
                {
                    return ToVector3(m_HeightScale) + new Vector3(0.0f, 1.75f, 0.0f);
                }
            case BoardType.Hexagonal:
                {
                    return ToVector3(m_HeightScale) + new Vector3(0.0f, 3.0f, 0.0f);
                }
        }
        return ToVector3(m_HeightScale);
    }

    public Vector3 ToVector3(float m_HeightScale)
    {
        return (new Vector3(XPos, (Height * m_HeightScale), YPos));
    }
}
