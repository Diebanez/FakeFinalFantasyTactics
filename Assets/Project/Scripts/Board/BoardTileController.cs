﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardTileController : MonoBehaviour {

    MeshRenderer m_Rendered;

    private void Start()
    {
        m_Rendered = GetComponentInChildren<MeshRenderer>();
        m_Rendered.material = BoardSettings.instance.LevelMaterials[(int)(transform.position.y / BoardSettings.instance.HeightScale)];
    }
}
