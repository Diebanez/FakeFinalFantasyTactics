﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour {

    [SerializeField]
    bool GenerateDebugVisualization = false;

    Dictionary<Vector2, GameObject> ObjectsDictionary = new Dictionary<Vector2, GameObject>();
    public Dictionary<GameObject, BoardTile> TileDictionary = new Dictionary<GameObject, BoardTile>();
    Queue<MeshRenderer> SelectedTiles = new Queue<MeshRenderer>();


    private void Awake()
    {
        BoardManager.instance.BoardGenerated += this.OnBoardGenerated;
    }

    public void OnBoardGenerated()
    {
        if (GenerateDebugVisualization)
        {
            GenerateDebugBoardGFX();
        }
        else
        {
            foreach (Node<BoardTile> TileNode in BoardManager.instance.BoardStructure.Nodes)
            {
                if (TileNode.Item.IsReachable)
                {
                    GameObject NewTile = Instantiate(BoardSettings.instance.TilePrefab, TileNode.Item.ToVector3(BoardSettings.instance.HeightScale), Quaternion.identity);
                    ObjectsDictionary.Add(new Vector2(TileNode.Item.XPos, TileNode.Item.YPos), NewTile);
                    NewTile.transform.parent = this.transform;
                    TileDictionary.Add(NewTile, TileNode.Item);
                }
                else
                {
                    GameObject NewTile = Instantiate(BoardSettings.instance.UnreachableTilePrefab, TileNode.Item.ToVector3(BoardSettings.instance.HeightScale), Quaternion.identity);
                    ObjectsDictionary.Add(new Vector2(TileNode.Item.XPos, TileNode.Item.YPos), NewTile);
                    NewTile.transform.parent = this.transform;
                    TileDictionary.Add(NewTile, TileNode.Item);
                }
            }
        }
    }

    public void SetSelectedTile(BoardTile Tile, Color SelectionColor)
    {
        if (ObjectsDictionary.ContainsKey(new Vector2(Tile.XPos, Tile.YPos)))
        {
            MeshRenderer ObjectRenderer = ObjectsDictionary[new Vector2(Tile.XPos, Tile.YPos)].GetComponentInChildren<MeshRenderer>();
            ObjectRenderer.material.color = SelectionColor;
            SelectedTiles.Enqueue(ObjectRenderer);
        }
    }

    public void CleanBoardVisualization()
    {
        while(SelectedTiles.Count > 0)
        {
            SelectedTiles.Dequeue().material.color = Color.white;
        }
    }

    public void GenerateDebugBoardGFX()
    {
        foreach (Node<BoardTile> n in BoardManager.instance.BoardStructure.Nodes)
        {
            GameObject newNode = Instantiate(BoardSettings.instance.TilePrefab, new Vector3(n.Item.XPos * 3, n.Item.Height * .5f, n.Item.YPos * 3), Quaternion.identity);
            newNode.transform.parent = this.transform;
            foreach (Edge<BoardTile> e in BoardManager.instance.BoardStructure.Edges)
            {
                GameObject NewLineRenderer = new GameObject("Edge");
                NewLineRenderer.transform.parent = newNode.transform;
                LineRenderer lr = NewLineRenderer.AddComponent<LineRenderer>();
                lr.SetPosition(0, new Vector3(e.StartNode.Item.XPos * 3, e.StartNode.Item.Height * .5f, e.StartNode.Item.YPos * 3));
                lr.SetPosition(1, new Vector3(e.EndNode.Item.XPos * 3, e.EndNode.Item.Height * .5f, e.EndNode.Item.YPos * 3));
                lr.startWidth = .1f;
                lr.endWidth = .1f;
            }
        }
    }
}
