﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoardType { Squared, Hexagonal}

public class BoardManager : Singleton<BoardManager> {
    public BoardType TypeOfBoard { get; private set; }
    public Graph<BoardTile> BoardStructure = new Graph<BoardTile>();
    public int BoardWidth { get; private set; }
    public float heightOffset = 1.75f;
    public float widthOffset = 1.5f;

    public event Action BoardGenerated;
    public event Action MovementDone;

    public BoardView View { get; private set; }    

    private void Awake()
    {
        TypeOfBoard = BoardSettings.instance.TypeOfBoard;
        BoardWidth = BoardSettings.instance.BoardWidth;
        GameObject ViewObject = new GameObject("BoardView");
        ViewObject.transform.parent = this.transform;
        View = ViewObject.AddComponent<BoardView>();
        this.BoardGenerated += GameManager.instance.OnBoardGenerated;
    }

    private void OnDestroy()
    {
        this.BoardGenerated -= GameManager.instance.OnBoardGenerated;
    }

    public void OnMovementDone()
    {
        if(MovementDone != null)
        {
            MovementDone();
        }
    }

    public void GenerateRandomBoard()
    {
        LoadingRandomBoard();
        //StartCoroutine(LoadingRandomBoard());
        /*
        for(int i = 0; i < BoardWidth; i++)
        {
            for(int j = 0; j < BoardWidth; j++)
            {
                BoardStructure.AddNode(new BoardTile(i, j, UnityEngine.Random.Range(0, 4)));
            }
        }
        foreach(Node<BoardTile> FirstNode in BoardStructure.Nodes)
        {
            foreach(Node<BoardTile> OtherNode in BoardStructure.Nodes)
            {
                if (FirstNode != OtherNode)
                {
                    try
                    {
                        if (!BoardStructure.EdgesDictionary.ContainsKey(FirstNode))
                        {
                            if (Mathf.Sqrt(Mathf.Pow(FirstNode.Item.XPos - OtherNode.Item.XPos, 2) + Mathf.Pow(FirstNode.Item.YPos - OtherNode.Item.YPos, 2)) == 1.0f)
                            {
                                BoardStructure.AddEdge(FirstNode.Item, OtherNode.Item, 1);
                            }
                        }
                        else
                        {
                            if (BoardStructure.EdgesDictionary[FirstNode].Count < 4)
                            {
                                if (Mathf.Sqrt(Mathf.Pow(FirstNode.Item.XPos - OtherNode.Item.XPos, 2) + Mathf.Pow(FirstNode.Item.YPos - OtherNode.Item.YPos, 2)) == 1.0f)
                                {
                                    BoardStructure.AddEdge(FirstNode.Item, OtherNode.Item, 1);
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Debug.Log(ex);
                    }
                }
            }
        }

        if(BoardGenerated != null)
        {
            BoardGenerated();
        }*/
        /*
        Queue<BoardTile> q = new Queue<BoardTile>();
        BoardTile newNode = new BoardTile(0, 0, UnityEngine.Random.Range(0, 4));
        BoardStructure.AddNode(newNode);
        q.Enqueue(BoardStructure.Nodes[0].Item);
        while (q.Count > 0)
        {
            BoardTile ActualProcessed = q.Dequeue();
            for (int i = 0; i < 6; i++)
            {
                if (BoardStructure.Nodes.Count < BoardWidth)
                {
                    BoardTile newTile = default(BoardTile);
                    switch (i)
                    {
                        case 0:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos, ActualProcessed.YPos - heightOffset, UnityEngine.Random.Range(0, 4));
                                break;
                            }
                        case 1:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos + widthOffset, ActualProcessed.YPos - (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                break;
                            }
                        case 2:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos + widthOffset, ActualProcessed.YPos + (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                break;
                            }
                        case 3:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos, ActualProcessed.YPos + heightOffset, UnityEngine.Random.Range(0, 4));
                                break;
                            }
                        case 4:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos - widthOffset, ActualProcessed.YPos + (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                break;
                            }
                        case 5:
                            {
                                newTile = new BoardTile(ActualProcessed.XPos - widthOffset, ActualProcessed.YPos - (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                break;
                            }
                    }
                    BoardStructure.AddNode(newTile);
                    q.Enqueue(BoardStructure.Nodes.Last().Item);
                }
            }
            foreach (Node<BoardTile> btNode in BoardStructure.Nodes)
            {
                if (btNode.Item.CompareTo(ActualProcessed) != 0 && Mathf.Sqrt(Mathf.Pow(btNode.Item.XPos - ActualProcessed.XPos, 2) + Mathf.Pow(btNode.Item.YPos - ActualProcessed.YPos, 2)) <= heightOffset)
                {
                    BoardStructure.AddEdge(btNode.Item, ActualProcessed, 1);
                    BoardStructure.AddEdge(ActualProcessed, btNode.Item, 1);
                }
                
            }
        }
        if (BoardGenerated != null)
        {
            BoardGenerated();
        }*/
    }    

    public void DrawKillableNodes(List<BoardTile> KillableNodes)
    {
        if (KillableNodes != null)
        {
            foreach (BoardTile tile in KillableNodes)
            {
                View.SetSelectedTile(tile, Color.red);
            }
        }
    }

    public void DrawReachableNodes(List<BoardTile> ReachableNodes)
    {
        if (ReachableNodes != null)
        {
            foreach (BoardTile tile in ReachableNodes)
            {
                View.SetSelectedTile(tile, Color.yellow);
            }
        }
    }

    void LoadingRandomBoard()
    {
        switch (TypeOfBoard)
        {
            case BoardType.Squared:
                {
                    for (int i = 0; i < BoardWidth; i++)
                    {
                        for (int j = 0; j < BoardWidth; j++)
                        {
                            BoardStructure.AddNode(new BoardTile(i, j, UnityEngine.Random.Range(0, 4)));
                            if(UnityEngine.Random.Range(0, 20) == 10)
                            {
                                BoardStructure.Nodes.Last().Item.IsReachable = false;
                                BoardStructure.Nodes.Last().Item.Height = 0;
                            }
                        }
                    }
                    foreach (Node<BoardTile> FirstNode in BoardStructure.Nodes)
                    {
                        foreach (Node<BoardTile> OtherNode in BoardStructure.Nodes)
                        {
                            if (FirstNode != OtherNode && FirstNode.Item.IsReachable && OtherNode.Item.IsReachable)
                            {
                                if (!BoardStructure.EdgesDictionary.ContainsKey(FirstNode))
                                {
                                    if (Mathf.Sqrt(Mathf.Pow(FirstNode.Item.XPos - OtherNode.Item.XPos, 2) + Mathf.Pow(FirstNode.Item.YPos - OtherNode.Item.YPos, 2)) == 1.0f)
                                    {
                                        BoardStructure.AddEdge(FirstNode.Item, OtherNode.Item, 1);
                                    }
                                }
                                else
                                {
                                    if (BoardStructure.EdgesDictionary[FirstNode].Count < 4)
                                    {
                                        if (Mathf.Sqrt(Mathf.Pow(FirstNode.Item.XPos - OtherNode.Item.XPos, 2) + Mathf.Pow(FirstNode.Item.YPos - OtherNode.Item.YPos, 2)) == 1.0f)
                                        {
                                            BoardStructure.AddEdge(FirstNode.Item, OtherNode.Item, 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
            case BoardType.Hexagonal:
                {
                    Queue<BoardTile> q = new Queue<BoardTile>();
                    BoardTile newNode = new BoardTile(0, 0, UnityEngine.Random.Range(0, 4));
                    BoardStructure.AddNode(newNode);
                    q.Enqueue(BoardStructure.Nodes[0].Item);

                    while (q.Count > 0)
                    {
                        BoardTile ActualProcessed = q.Dequeue();
                        for (int i = 0; i < 6; i++)
                        {
                            if (BoardStructure.Nodes.Count < BoardWidth * BoardWidth)
                            {
                                BoardTile newTile = default(BoardTile);
                                switch (i)
                                {
                                    case 0:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos, ActualProcessed.YPos - heightOffset, UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                    case 1:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos + widthOffset, ActualProcessed.YPos - (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                    case 2:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos + widthOffset, ActualProcessed.YPos + (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                    case 3:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos, ActualProcessed.YPos + heightOffset, UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                    case 4:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos - widthOffset, ActualProcessed.YPos + (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                    case 5:
                                        {
                                            newTile = new BoardTile(ActualProcessed.XPos - widthOffset, ActualProcessed.YPos - (heightOffset / 2), UnityEngine.Random.Range(0, 4));
                                            break;
                                        }
                                }
                                if (BoardStructure.GetNode(newTile) == null)
                                {
                                    BoardStructure.AddNode(newTile);
                                    if (UnityEngine.Random.Range(0, 20) == 10)
                                    {
                                        BoardStructure.Nodes.Last().Item.IsReachable = false;
                                        BoardStructure.Nodes.Last().Item.Height = 0;
                                    }
                                    q.Enqueue(BoardStructure.Nodes.Last().Item);
                                }
                                //yield return new WaitForEndOfFrame();
                                //yield return null;
                            }
                        }
                        foreach (Node<BoardTile> btNode in BoardStructure.Nodes)
                        {
                            if (btNode.Item.CompareTo(ActualProcessed) != 0 && Mathf.Sqrt(Mathf.Pow(btNode.Item.XPos - ActualProcessed.XPos, 2) + Mathf.Pow(btNode.Item.YPos - ActualProcessed.YPos, 2)) <= heightOffset && btNode.Item.IsReachable && ActualProcessed.IsReachable)
                            {
                                BoardStructure.AddEdge(btNode.Item, ActualProcessed, 1);
                                BoardStructure.AddEdge(ActualProcessed, btNode.Item, 1);
                            }

                        }
                        //yield return new WaitForEndOfFrame();
                        //yield return null;                        
                    }
                    break;
                }
        }
        
        if (BoardGenerated != null)
        {
            BoardGenerated();
        }
        //yield return new WaitForEndOfFrame();
        //yield return null;
    }
        
    public void GenerateBoardBySeed(string seed)
    {
        throw new NotImplementedException();
    }
}
