﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    [SerializeField]
    Text PlayerNameField;
    [SerializeField]
    Text ClassField;
    [SerializeField]
    Text HealthField;
    [SerializeField]
    Text DamageField;
    [SerializeField]
    Text AttackRangeField;
    [SerializeField]
    Text JumpHeightField;
    [SerializeField]
    Text MovementRangeField;

    private void Awake()
    {
        TurnManager.instance.NewTurn += OnNewTurn;
        this.gameObject.SetActive(false);
        BoardManager.instance.BoardGenerated += this.OnBoardGenerated;
    }

    void OnBoardGenerated()
    {
        this.gameObject.SetActive(true);
    }

    public void OnNewTurn(PlayerController ActualPlayer)
    {
        PlayerNameField.text = ActualPlayer.gameObject.name;
        ClassField.text = ActualPlayer.MyDatas.Class.ToString();
        HealthField.text = ActualPlayer.MyDatas.Life.ToString();
        DamageField.text = ActualPlayer.MyDatas.Damage.ToString();
        AttackRangeField.text = ActualPlayer.MyDatas.AttackRange.ToString();
        JumpHeightField.text = ActualPlayer.MyDatas.JumpHeight.ToString();
        MovementRangeField.text = ActualPlayer.MyDatas.MovementRange.ToString();
    }

    public void NewTurnButtonClick()
    {
        if (TurnManager.instance.ActualAction != Actions.Moving && TurnManager.instance.ActualAction != Actions.Attacking)
        {
            PlayerManager.instance.StartNewTurn();
        }
    }

    public void MoveButtonClick()
    {
        if (TurnManager.instance.ActualAction != Actions.Moving)
        {
            InputManager.instance.OnMovementSelected();
        }
    }

    public void AttackButtonClick()
    {
        if(TurnManager.instance.ActualAction != Actions.Attacking)
        {
            InputManager.instance.OnAttackSelected();
        }
    }
}
