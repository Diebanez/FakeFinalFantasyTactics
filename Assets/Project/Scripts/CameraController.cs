﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    float TotalTime = 1.0f;
    [SerializeField]
    float ZoomValue = 1.0f;
    [SerializeField]
    float MaxCameraSize = 10.0f;
    [SerializeField]
    float MinCameraSize = 5.0f;
    [SerializeField]
    float DragSensibility = 1.0f;

    private Vector3 StartPosition = Vector3.zero;
    private Vector3 TargetPosition = Vector3.zero;
    private Quaternion StartRotation = Quaternion.identity;
    private Quaternion TargetRotation = Quaternion.identity;
    Vector3 LastMousePosition;
    float TimePassed = 0.0f;
    bool IsCameraMoving = false;

    private void Awake()
    {
        TurnManager.instance.NewTurn += this.OnNewTurn;
        TurnManager.instance.MovementDone += this.OnMovementDone;
        InputManager.instance.ZoomIn += this.OnZoomIn;
        InputManager.instance.ZoomOut += this.OnZoomOut;
        InputManager.instance.CameraMovementStart += this.OnCameraMovementStart;
        InputManager.instance.CameraMovementDone += this.OnCameraMovementDone;
    }

    public void OnNewTurn(PlayerController ActualPlayer)
    {
        UpdateCameraPosition(ActualPlayer);
    }

    public void OnMovementDone(PlayerController ActualPlayer)
    {
        UpdateCameraPosition(ActualPlayer);
    }

    void UpdateCameraPosition(PlayerController ActualPlayer)
    {
        if (BoardSettings.instance.TypeOfBoard == BoardType.Squared)
        {
            if (ActualPlayer.transform.position.x <= BoardSettings.instance.BoardWidth / 2 && ActualPlayer.transform.position.z <= BoardSettings.instance.BoardWidth / 2)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x - 16.0f, 25.0f, ActualPlayer.transform.position.z - 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 45.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x <= BoardSettings.instance.BoardWidth / 2 && ActualPlayer.transform.position.z > BoardSettings.instance.BoardWidth / 2)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x - 16.0f, 25.0f, ActualPlayer.transform.position.z + 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 135.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x > BoardSettings.instance.BoardWidth / 2 && ActualPlayer.transform.position.z > BoardSettings.instance.BoardWidth / 2)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x + 16.0f, 25.0f, ActualPlayer.transform.position.z + 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 225.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x > BoardSettings.instance.BoardWidth / 2 && ActualPlayer.transform.position.z <= BoardSettings.instance.BoardWidth / 2)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x + 16.0f, 25.0f, ActualPlayer.transform.position.z - 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 315.0f, 0.0f));
            }
        }
        else
        {
            if (ActualPlayer.transform.position.x <= 0 && ActualPlayer.transform.position.z <= 0)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x - 16.0f, 25.0f, ActualPlayer.transform.position.z - 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 45.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x <= 0 && ActualPlayer.transform.position.z > 0)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x - 16.0f, 25.0f, ActualPlayer.transform.position.z + 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 135.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x > 0 && ActualPlayer.transform.position.z > 0)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x + 16.0f, 25.0f, ActualPlayer.transform.position.z + 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 225.0f, 0.0f));
            }
            else if (ActualPlayer.transform.position.x > 0 && ActualPlayer.transform.position.z <= 0)
            {
                TargetPosition = new Vector3(ActualPlayer.transform.position.x + 16.0f, 25.0f, ActualPlayer.transform.position.z - 16.0f);
                TargetRotation = Quaternion.Euler(new Vector3(45.0f, 315.0f, 0.0f));
            }
        }
        StartRotation = transform.rotation;
        StartPosition = transform.position;
        TimePassed = 0.0f;
    }

    void OnZoomIn()
    {
        if(Camera.main.orthographicSize - ZoomValue < MinCameraSize)
        {
            Camera.main.orthographicSize = MinCameraSize;
        }
        else
        {
            Camera.main.orthographicSize -= ZoomValue;
        }
    }

    void OnZoomOut()
    {
        if (Camera.main.orthographicSize + ZoomValue > MaxCameraSize)
        {
            Camera.main.orthographicSize = MaxCameraSize;
        }
        else
        {
            Camera.main.orthographicSize += ZoomValue;
        }
    }

    void OnCameraMovementStart()
    {
        IsCameraMoving = true;
    }

    void OnCameraMovementDone()
    {
        IsCameraMoving = false;
    }

    private void Start()
    {
        LastMousePosition = Input.mousePosition;
    }

    private void Update()
    {
        if(transform.position != TargetPosition)
        {            
            transform.rotation = Quaternion.Lerp(StartRotation, TargetRotation, TimePassed / TotalTime);
            transform.position = Vector3.Lerp(StartPosition, TargetPosition, TimePassed / TotalTime);
            //transform.LookAt(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer].transform.position);
            TimePassed += Time.deltaTime;
        }
        if (IsCameraMoving)
        {
            if(Input.mousePosition != LastMousePosition)
            {
                Vector3 DragMovement = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.ScreenToWorldPoint(LastMousePosition));
                TargetPosition -= DragMovement * DragSensibility;
            }            
        }

        LastMousePosition = Input.mousePosition;
    }
}
