﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager> {
    public List<PlayerController> Players { get; private set; }
    public event Action<PlayerController> NewPlayerTurn;
    public event Action<int> TeamDied;

    public int ActualPlayer = -1;

    private void Awake()
    {
        BoardManager.instance.BoardGenerated += LoadPlayerData;
    }

    public void StartNewTurn()
    {
        if(ActualPlayer < Players.Count - 1)
        {
            ActualPlayer++;
        }
        else
        {
            ActualPlayer = 0;
        }
        if (NewPlayerTurn != null)
        {
            NewPlayerTurn(Players[ActualPlayer]);
        }

    }

    public void LoadPlayerData()
    {
        Players = new List<PlayerController>();
        foreach (PlayerData m_PlayerData in DataManager.instance.PlayerDatas)
        {
            GameObject NewPlayer;
            if (m_PlayerData.Team < 0)
            {
                if (m_PlayerData.Class == Classes.Sniper)
                {
                    NewPlayer = Instantiate(Resources.Load("Prefabs/TeamOne/PlayerSniper") as GameObject);
                }
                else
                {
                    NewPlayer = Instantiate(Resources.Load("Prefabs/TeamOne/PlayerScout") as GameObject);
                }
            }
            else
            {
                if (m_PlayerData.Class == Classes.Sniper)
                {
                    NewPlayer = Instantiate(Resources.Load("Prefabs/TeamTwo/PlayerSniper") as GameObject);
                }
                else
                {
                    NewPlayer = Instantiate(Resources.Load("Prefabs/TeamTwo/PlayerScout") as GameObject);
                }
            }

            NewPlayer.name = m_PlayerData.Name;
            Players.Add(NewPlayer.AddComponent<PlayerController>());
            Players.Last().MyDatas.UpdateDatas(m_PlayerData);
            /*
            Players.Last().MyDatas.Health = m_PlayerData.Health;
            Players.Last().MyDatas.MovementRange = m_PlayerData.MovementRange;
            Players.Last().MyDatas.AttackRange = m_PlayerData.AttackRange;
            Players.Last().MyDatas.JumpHeigt = m_PlayerData.JumpHeigt;
            Players.Last().MyDatas.Damage = m_PlayerData.Damage;
            Players.Last().MyDatas.Experience = m_PlayerData.Experience;
            Players.Last().MyDatas.team = m_PlayerData.team;
            */
            Players.Last().MyDatas.Name = m_PlayerData.Name;
            NewPlayer.gameObject.tag = "Player";

            switch (BoardManager.instance.TypeOfBoard)
            {
                case BoardType.Squared:
                    {
                        switch (Players.Count)
                        {
                            case 1:
                                {
                                    Players.Last().SetPosition(new BoardTile(0.0f, 0.0f, 4));
                                    break;
                                }
                            case 2:
                                {
                                    Players.Last().SetPosition(new BoardTile(BoardManager.instance.BoardWidth - 1, 0.0f, 4));
                                    break;
                                }
                            case 3:
                                {
                                    Players.Last().SetPosition(new BoardTile(BoardManager.instance.BoardWidth - 1, BoardManager.instance.BoardWidth - 1, 4));
                                    break;
                                }
                            case 4:
                                {
                                    Players.Last().SetPosition(new BoardTile(0.0f, BoardManager.instance.BoardWidth - 1, 4));
                                    break;
                                }

                        }
                        break;
                    }
                case BoardType.Hexagonal:
                    {
                        switch (Players.Count)
                        {
                            case 1:
                                {
                                    Players.Last().SetPosition(new BoardTile(-10.5f, -6.125f, 4));
                                    break;
                                }
                            case 2:
                                {
                                    Players.Last().SetPosition(new BoardTile(10.5f, -6.125f, 4));
                                    break;
                                }
                            case 3:
                                {
                                    Players.Last().SetPosition(new BoardTile(10.5f, 6.125f, 4));
                                    break;
                                }
                            case 4:
                                {
                                    Players.Last().SetPosition(new BoardTile(-10.5f, 6.125f, 4));
                                    break;
                                }
                                
                        }
                        break;
                    }
            }
            Players.Last().PlayerDie += this.OnPlayerDie;
        }
        StartNewTurn();
    }

    public void OnPlayerDie(PlayerController DiedPlayer)
    {
        int IndexOfPlayer = Players.IndexOf(DiedPlayer);
        Players.RemoveAt(IndexOfPlayer);
        if (IndexOfPlayer <= ActualPlayer)
        {
            if (ActualPlayer > 0) {
                ActualPlayer--;
            }
            else
            {
                ActualPlayer = Players.Count - 1;
            }
        }
        CheckTeamVictory();
    }

    public void CheckTeamVictory()
    {
        bool TeamOneFounded = false;
        bool TeamTwoFounded = false;

        foreach(PlayerController pl in Players)
        {
            if(pl.MyDatas.Team < 0)
            {
                TeamOneFounded = true;
            }
            else
            {
                TeamTwoFounded = true;
            }
            if(TeamOneFounded && TeamTwoFounded)
            {
                break;
            }
        }
        if (!TeamOneFounded)
        {
            OnTeamDied(1);
        }else if (!TeamTwoFounded)
        {
            OnTeamDied(-1);
        }
    }

    void OnTeamDied(int VictoryTeam)
    {
        if(TeamDied != null)
        {
            TeamDied(VictoryTeam);
        }
    }
}
