﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour {

    PlayerController MyController;
    Animator MyAnimator;
    public GameObject ProjectilePrefab;
    public Transform ShootMuzzle;

    private void Start()
    {
        MyController = GetComponent<PlayerController>();
        MyAnimator = GetComponentInChildren<Animator>();
        MyController.AttackExecuted += this.OnAttack;
        MyController.MoveStart += this.OnMovementStart;
        MyController.MoveDone += this.OnMovementDone;
        MyController.PlayerDie += this.OnDie;
        MyController.TakeDamage += this.OnDamage;
    }

    void OnMovementStart(PlayerController ActualPlayer)
    {
        MyAnimator.SetBool("IsMoving", true);
    }

    void OnMovementDone(PlayerController ActualPlayer)
    {
        MyAnimator.SetBool("IsMoving", false);
    }

    void OnAttack(PlayerController ActualPlayer, PlayerController TargetPlayer)
    {
        MyAnimator.SetTrigger("Attack");
        if (ActualPlayer.MyDatas.Class == Classes.Sniper)
        {
            GameObject NewProjectile = Instantiate(ProjectilePrefab, ShootMuzzle.position, Quaternion.identity);
            ProjectileController projectile = NewProjectile.AddComponent<ProjectileController>();
            projectile.StartPosition = ShootMuzzle.position;
            projectile.TargetPosition = TargetPlayer.transform.position;
        }
    }

    void OnDie(PlayerController ActualPlayer)
    {
        MyAnimator.SetTrigger("Die");
    }

    void OnDamage(PlayerController ActualPlayer)
    {
        MyAnimator.SetTrigger("Damage");
    }
}
