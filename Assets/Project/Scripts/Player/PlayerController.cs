﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public event Action<PlayerController> PlayerDie;
    public event Action<PlayerController> MoveDone;
    public event Action<PlayerController> MoveStart;
    public event Action<PlayerController, PlayerController> AttackExecuted;
    public event Action<PlayerController> TakeDamage;
    
    public PlayerData MyDatas = new PlayerData();
    List<BoardTile> Path;
    int ActualPosition;
    float TimePassed;

    Graph<BoardTile> MyMovementGraph;

    List<BoardTile> SelectedPath;
    List<BoardTile> ReachableNodes;
    List<BoardTile> KillableNodes;

    List<List<BoardTile>> PossiblePaths;

    Renderer MyRenderer;

    public bool HasMoved = false;
    public bool HasAttacked = false;

    private void Awake()
    {
        InputManager.instance.MovementSelected += this.OnMovementSelected;
        InputManager.instance.AttackSelected += this.OnAttackSelected;
        InputManager.instance.TileSelected += this.OnTileSelected;
        InputManager.instance.TileClicked += this.OnTileClicked;
        InputManager.instance.PlayerClicked += this.OnPlayerClicked;
        InputManager.instance.PlayerSelected += this.OnPlayerSelected;
        InputManager.instance.PlayerDeSelected += this.OnPlayerDeselected;
        this.transform.position = new Vector3(MyDatas.Position.XPos, MyDatas.Position.Height * .5f, MyDatas.Position.YPos);
        TurnManager.instance.NewTurn += this.StartPlayerTurn;
        MyRenderer = GetComponentInChildren<Renderer>();
    }

    public void SetPosition(BoardTile position)
    {
        MyDatas.Position = position;
        this.transform.position = position.ToPlayerVector3(BoardSettings.instance.TypeOfBoard, BoardSettings.instance.HeightScale);
    }

    public void Update()
    {
        if(Path != null)
        {
            if(ActualPosition >= Path.Count)
            {
                this.OnMovementDone();
                MyDatas.Position.XPos = transform.position.x;
                MyDatas.Position.YPos = transform.position.z;
                BoardManager.instance.View.CleanBoardVisualization();
                HasMoved = true;
                Path = null;                
                BoardManager.instance.BoardStructure.SetDijkstraSource(this.MyDatas.Position);                
            }
            else
            {
                if(transform.position.x == Path[ActualPosition].XPos && transform.position.z == Path[ActualPosition].YPos)
                {
                    ActualPosition++;
                    TimePassed = 0.0f;
                }
                else
                {
                    Vector3 heading = Path[ActualPosition].ToPlayerVector3(BoardSettings.instance.TypeOfBoard, BoardSettings.instance.HeightScale) - Path[ActualPosition - 1].ToPlayerVector3(BoardSettings.instance.TypeOfBoard, BoardSettings.instance.HeightScale);
                    //Setta l'heading a 0 se vuoi ignorare l'altezza come parametro
                    //heading.y = 0;
                    float distance = heading.magnitude;
                    Vector3 direction = heading / distance;
                    //    if(Path[ActualPosition - 1].XPos == Path[ActualPosition].XPos)
                    //    {
                    //        if(Path[ActualPosition - 1].YPos < Path[ActualPosition].YPos)
                    //        {
                    //            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 0.0f, transform.eulerAngles.z);
                    //        }
                    //        else
                    //        {
                    //            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 180.0f, transform.eulerAngles.z);
                    //        }
                    //    }else if(Path[ActualPosition - 1].YPos == Path[ActualPosition].YPos)
                    //    {
                    //        if (Path[ActualPosition - 1].XPos < Path[ActualPosition].XPos)
                    //        {
                    //            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 90.0f, transform.eulerAngles.z);
                    //        }
                    //        else
                    //        {
                    //            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, 270.0f, transform.eulerAngles.z);
                    //        }
                    //    }
                    transform.rotation = Quaternion.Euler(transform.eulerAngles.x, Quaternion.LookRotation(direction).eulerAngles.y, transform.eulerAngles.z);
                    transform.position = Vector3.Lerp(Path[ActualPosition - 1].ToPlayerVector3(BoardSettings.instance.TypeOfBoard, BoardSettings.instance.HeightScale), Path[ActualPosition].ToPlayerVector3(BoardSettings.instance.TypeOfBoard, BoardSettings.instance.HeightScale), TimePassed / .25f);
                    TimePassed += Time.deltaTime;
                }
            }
        }
    }

    public void OnMovementDone()
    {
        if(MoveDone != null)
        {
            MoveDone(this);
        }
        BoardManager.instance.OnMovementDone();
    }

    public void StartPlayerTurn(PlayerController ActualPlayer)
    {
        if(ActualPlayer != this)
        {
            MyRenderer.material.SetFloat("_Outline", .0f);
            return;
        }
        HasMoved = false;
        HasAttacked = false;

        CalculateMyMovementGraph();
        MyMovementGraph.SetDijkstraSource(ActualPlayer.MyDatas.Position);
        BoardManager.instance.BoardStructure.SetDijkstraSource(this.MyDatas.Position);
        BoardManager.instance.View.CleanBoardVisualization();
        BoardManager.instance.View.SetSelectedTile(ActualPlayer.MyDatas.Position, Color.black);
        MyRenderer.material.SetColor("_OutlineColor", Color.green);
        MyRenderer.material.SetFloat("_Outline", .05f);
        
    }

    public void OnPlayerDeselected(GameObject m_Player)
    {
        if (m_Player != PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer].gameObject)
        {
            if (m_Player == this.gameObject)
            {
                MyRenderer.material.SetFloat("_Outline", .0f);
            }
        }
    }

    public void OnPlayerSelected(GameObject m_Player)
    {
        if (m_Player != PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer].gameObject)
        {
            if (m_Player == this.gameObject)
            {
                MyRenderer.material.SetColor("_OutlineColor", Color.red);
                MyRenderer.material.SetFloat("_Outline", .05f);
            }
        }
    }

    public void DealDamage(int Damage)
    {
        MyDatas.Life -= Damage;
        if(MyDatas.Life <= 0)
        {
            if(PlayerDie != null)
            {
                PlayerDie(this);
                //Destroy(this.gameObject);
            }
        }
        else
        {
            if(TakeDamage != null)
            {
                TakeDamage(this);
            }
        }
    }

    void CalculateMyMovementGraph()
    {
        MyMovementGraph = new Graph<BoardTile>();
        foreach(Node<BoardTile> n in BoardManager.instance.BoardStructure.Nodes)
        {
            bool founded = false;
            foreach (PlayerController pl in PlayerManager.instance.Players)
            {
                if (pl != this && n.Item.CompareTo(pl.MyDatas.Position) == 0)
                {
                    founded = true;
                    break;
                }
            }
            if (!founded) {
                MyMovementGraph.AddNode(n.Item);
            }
        }
        foreach(Edge<BoardTile> ed in BoardManager.instance.BoardStructure.Edges)
        {
            bool founded = false;
            foreach (PlayerController pl in PlayerManager.instance.Players)
            {
                if (pl != this && (ed.StartNode.Item.CompareTo(pl.MyDatas.Position) == 0 || ed.EndNode.Item.CompareTo(pl.MyDatas.Position) == 0))
                {
                    founded = true;
                    break;
                }
            }
            if (!founded && ed.EndNode.Item.Height - ed.StartNode.Item.Height <= MyDatas.JumpHeight)
            {
                MyMovementGraph.AddEdge(ed.StartNode.Item, ed.EndNode.Item, ed.Width);
            }
        }
    }

    public void SetPath(List<BoardTile> NewPath)
    {
        HasMoved = true;
        ActualPosition = 1;
        TimePassed = 0.0f;
        Path = NewPath;
    }

    public void OnMovementSelected(PlayerController ActualPlayer)
    {
        if (PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer] == this)
        {
            if (!this.HasMoved)
            {
                TurnManager.instance.ActualAction = Actions.SelectingMovement;
                BoardManager.instance.View.CleanBoardVisualization();
                ReachableNodes = new List<BoardTile>();
                PossiblePaths = new List<List<BoardTile>>();
                foreach (Node<BoardTile> N in MyMovementGraph.Nodes)
                {
                    if (N.Item.CompareTo(this.MyDatas.Position) != 0)
                    {
                        List<BoardTile> CheckingPath = MyMovementGraph.GetShortestPathTo(N.Item);
                        if (CheckingPath != null && CheckingPath.Count <= this.MyDatas.MovementRange && CheckingPath.Count > 0)
                        {
                            ReachableNodes.Add(N.Item);
                            PossiblePaths.Add(CheckingPath);
                        }
                    }
                }
                BoardManager.instance.DrawReachableNodes(ReachableNodes);
                BoardManager.instance.View.SetSelectedTile(this.MyDatas.Position, Color.black);
            }
        }
    }

    public void OnAttackSelected(PlayerController ActualPlayer)
    {
        if (PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer] == this)
        {
            if (!this.HasAttacked)
            {
                TurnManager.instance.ActualAction = Actions.SelectingAttack;
                BoardManager.instance.View.CleanBoardVisualization();
                KillableNodes = new List<BoardTile>();
                foreach (Node<BoardTile> N in BoardManager.instance.BoardStructure.Nodes)
                {
                    if (N.Item.CompareTo(MyDatas.Position) != 0)
                    {
                        List<BoardTile> CheckingPath = BoardManager.instance.BoardStructure.GetShortestPathTo(N.Item);
                        if (CheckingPath != null)
                        {
                            if (CheckingPath.Count <= this.MyDatas.AttackRange && CheckingPath.Count > 0)
                            {
                                KillableNodes.Add(N.Item);
                            }
                        }
                    }
                }
                BoardManager.instance.DrawKillableNodes(KillableNodes);
                BoardManager.instance.View.SetSelectedTile(this.MyDatas.Position, Color.black);
            }
        }
    }

    void OnTileSelected(GameObject Tile)
    {
        if (PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer] == this)
        {
            if (!this.HasMoved)
            {
                if (TurnManager.instance.ActualAction == Actions.SelectingMovement)
                {
                    
                    if (BoardManager.instance.View.TileDictionary.ContainsKey(Tile))
                    {
                        SelectedPath = null;
                        foreach (List<BoardTile> CheckingPath in PossiblePaths)
                        {
                            if (CheckingPath.Last() == BoardManager.instance.View.TileDictionary[Tile])
                            {
                                SelectedPath = CheckingPath;
                                break;
                            }
                        }
                        BoardManager.instance.View.CleanBoardVisualization();
                        BoardManager.instance.DrawReachableNodes(ReachableNodes);
                        if (SelectedPath != null)
                        {
                            foreach (BoardTile tile in SelectedPath)
                            {
                                BoardManager.instance.View.SetSelectedTile(tile, Color.green);
                            }                            
                        }
                        BoardManager.instance.View.SetSelectedTile(this.MyDatas.Position, Color.black);
                        //}
                    }
                }
            }
        }
    }

    void OnTileClicked(GameObject Tile)
    {
        if (PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer] == this)
        {
            if (!this.HasMoved)
            {
                if (TurnManager.instance.ActualAction == Actions.SelectingMovement)
                {
                    TurnManager.instance.ActualAction = Actions.Moving;
                    if (SelectedPath != null)
                    {
                        BoardManager.instance.View.CleanBoardVisualization();
                        BoardManager.instance.DrawReachableNodes(ReachableNodes);
                        foreach (BoardTile tile in SelectedPath)
                        {
                            BoardManager.instance.View.SetSelectedTile(tile, Color.blue);
                            //Debug.Log(tile.XPos + ";" + tile.YPos);
                        }
                        BoardManager.instance.View.SetSelectedTile(this.MyDatas.Position, Color.black);
                        this.SetPath(SelectedPath);
                        if(MoveStart != null)
                        {
                            MoveStart(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer]);
                        }
                    }
                    else
                    {
                        TurnManager.instance.ActualAction = Actions.SelectingMovement;
                    }
                }
            }
        }
    }

    void OnPlayerClicked(GameObject Player)
    {
        if(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer] == this && Player != this.gameObject)
        {
            if (!this.HasAttacked) {
                if(TurnManager.instance.ActualAction == Actions.SelectingAttack){
                    TurnManager.instance.ActualAction = Actions.Attacking;
                    PlayerController other = Player.GetComponent<PlayerController>();
                    if (other.MyDatas.Team != this.MyDatas.Team)
                    {
                        bool founded = false;
                        foreach (BoardTile BoardPosition in KillableNodes)
                        {
                            if (other.MyDatas.Position.CompareTo(BoardPosition) == 0)
                            {
                                founded = true;
                                break;
                            }
                        }
                        if (founded)
                        {
                            Vector3 heading = Player.transform.position - transform.position;
                            float distance = heading.magnitude;
                            Vector3 direction = heading / distance;
                            transform.rotation = Quaternion.Euler(transform.eulerAngles.x, Quaternion.LookRotation(direction).eulerAngles.y, transform.eulerAngles.z);
                            other.DealDamage(MyDatas.Damage);
                            this.MyDatas.Experience++;
                            BoardManager.instance.View.CleanBoardVisualization();
                            this.HasAttacked = true;
                            if(AttackExecuted != null)
                            {
                                AttackExecuted(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer], other);
                            }
                            TurnManager.instance.ActualAction = Actions.Selecting;
                        }
                        else
                        {
                            TurnManager.instance.ActualAction = Actions.SelectingAttack;
                        }
                    }
                    else
                    {
                        TurnManager.instance.ActualAction = Actions.SelectingAttack;
                    }
                }
            }
        }
    }
}
