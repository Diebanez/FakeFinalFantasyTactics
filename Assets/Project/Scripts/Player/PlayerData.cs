﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Classes { Scout, Sniper}

[System.Serializable]
public class PlayerData {

    public BoardTile Position { get; set; }
    public int Health {
        get
        {
            return m_Health;
        }
        set{
            m_Health = value;
            Life = value;
            InspectorHealth = value;
        }
    }
    int m_Health;
    Classes m_Class;
    public string Name;
    public int MovementRange;
    public int AttackRange;
    public int Damage;
    public int JumpHeight;
    public int Team;
    public int InspectorHealth;
    public int Life { get; set; }
    public int Experience { get; set; }
    public Classes InspectorClassValue;
    public Classes Class
    {
        get
        {
            return m_Class;
        }
        set
        {
            switch (value)
            {
                case Classes.Scout:
                    {
                        AttackRange = 2;
                        MovementRange = 5;
                        m_Class = value;
                        break;
                    }
                case Classes.Sniper:
                    {
                        AttackRange = 4;
                        MovementRange = 3;
                        m_Class = value;
                        break;
                    }
            }
            InspectorClassValue = m_Class;
        }
    }
    public PlayerData()
    {
        this.Health = 10;
        this.JumpHeight = 2;
        this.Damage = 3;
        this.Experience = 0;
        this.Position = new BoardTile(0, 0, 0);
        this.Name = "Player";
        this.Team = -1;
        this.Class = Classes.Scout;
    }

    public void UpdateDatas(PlayerData Source)
    {
        Experience = Source.Experience;
        Health = Source.Health;
        MovementRange = Source.MovementRange;
        AttackRange = Source.AttackRange;
        Damage = Source.Damage;
        JumpHeight = Source.JumpHeight;
        this.Team = Source.Team;
        this.Class = Source.Class;
    }
}
