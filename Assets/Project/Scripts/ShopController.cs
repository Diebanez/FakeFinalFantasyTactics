﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopController : MonoBehaviour {
    [SerializeField]
    TextMeshProUGUI PlayerName;
    [SerializeField]
    TextMeshProUGUI PlayerClass;
    [SerializeField]
    TextMeshProUGUI PlayerTeam;
    [SerializeField]
    TextMeshProUGUI PlayerActualHealth;
    [SerializeField]
    TextMeshProUGUI PlayerActualDamage;
    [SerializeField]
    TextMeshProUGUI PlayerActualJumpHeight;
    [SerializeField]
    TMP_Dropdown PlayerSelected;
    [SerializeField]
    TextMeshProUGUI Experience;
    [SerializeField]
    TextMeshProUGUI AdditionalHealth;
    [SerializeField]
    TextMeshProUGUI AdditionalDamage;
    [SerializeField]
    TextMeshProUGUI AdditionalJumpHeight;

    private void Start()
    {
        foreach(PlayerData pl in DataManager.instance.PlayerDatas)
        {
            PlayerSelected.options.Add(new TMP_Dropdown.OptionData(pl.Name));
        }
        PlayerSelected.value = 0;

        ShowData(DataManager.instance.PlayerDatas[0]);
    }

    public void OnBuyClick()
    {
        DataManager.instance.PlayerDatas[PlayerSelected.value].Experience = int.Parse(Experience.text);
        DataManager.instance.PlayerDatas[PlayerSelected.value].Health += int.Parse(AdditionalHealth.text);
        DataManager.instance.PlayerDatas[PlayerSelected.value].Damage += int.Parse(AdditionalDamage.text);
        DataManager.instance.PlayerDatas[PlayerSelected.value].JumpHeight += int.Parse(AdditionalJumpHeight.text);
        AdditionalHealth.text = "0";
        AdditionalDamage.text = "0";
        AdditionalJumpHeight.text = "0";
        ShowData(DataManager.instance.PlayerDatas[PlayerSelected.value]);
    }

    void ShowData(PlayerData Player)
    {
        PlayerName.text = Player.Name;
        PlayerClass.text = Player.Class.ToString();
        if (Player.Team < 0)
        {
            PlayerTeam.text = "Team 1";
        }
        else
        {
            PlayerTeam.text = "Team 2";
        }
        PlayerActualHealth.text = Player.Health.ToString();
        PlayerActualDamage.text = Player.Damage.ToString();
        PlayerActualJumpHeight.text = Player.JumpHeight.ToString();
        Experience.text = Player.Experience.ToString();
    }

    public void OnPlayerSelected()
    {
        ShowData(DataManager.instance.PlayerDatas[PlayerSelected.value]);
    }

}
