﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum Actions { Moving, Attacking, Selecting, SelectingMovement, SelectingAttack }
public class TurnManager : Singleton<TurnManager> {
    public event Action<PlayerController> NewTurn;
    public event Action<PlayerController> MovementDone;
    public Actions ActualAction;

    private void Awake()
    {
        PlayerManager.instance.NewPlayerTurn += OnNewPlayerTurn;
        BoardManager.instance.MovementDone += this.OnMovementDone;
    }

    void OnNewPlayerTurn(PlayerController Player)
    {
        ActualAction = Actions.Selecting;
        if(NewTurn != null)
        {
            NewTurn(Player);
        }
    }

    void OnMovementDone()
    {
        if(MovementDone != null)
        {
            MovementDone(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer]);
        }
        ActualAction = Actions.Selecting;
    }

}
