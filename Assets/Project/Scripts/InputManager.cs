﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager> {
    public event Action<GameObject> TileClicked;
    public event Action<GameObject> TileSelected;
    public event Action<GameObject> PlayerSelected;
    public event Action<GameObject> PlayerDeSelected;
    public event Action<GameObject> PlayerClicked;
    public event Action<PlayerController> MovementSelected;
    public event Action<PlayerController> AttackSelected;
    public event Action ZoomIn;
    public event Action ZoomOut;
    public event Action CameraMovementStart;
    public event Action CameraMovementDone;

    GameObject SelectedPlayer;

    private void LateUpdate()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;        

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.tag == "Tile")
                {
                    if(TileClicked != null)
                    {
                        TileClicked(hit.transform.gameObject);
                    }
                }
                else if (hit.collider.tag == "Player")
                {
                    if (PlayerClicked != null)
                    {
                        PlayerClicked(hit.transform.gameObject);
                    }
                }
            }
        }
        else
        {
            if (Physics.Raycast(ray, out hit, 100))
            {
                if (hit.collider.tag == "Tile")
                {
                    if (SelectedPlayer != null)
                    {
                        if (PlayerDeSelected != null)
                        {
                            PlayerDeSelected(SelectedPlayer);
                        }
                        SelectedPlayer = null;
                    }
                    if (TileSelected != null)
                    {
                        TileSelected(hit.transform.gameObject);
                    }
                }
                else if (hit.collider.tag == "Player")
                {
                    if (SelectedPlayer != hit.transform.gameObject)
                    {
                        SelectedPlayer = hit.transform.gameObject;
                        if (PlayerSelected != null)
                        {
                            PlayerSelected(hit.transform.gameObject);
                        }
                    }
                }
            }
            else
            {
                if(SelectedPlayer != null)
                {
                    if(PlayerDeSelected != null)
                    {
                        PlayerDeSelected(SelectedPlayer);
                    }
                    SelectedPlayer = null;
                }
            }
        }
        if (Input.GetMouseButtonDown(2))
        {
            if(CameraMovementStart != null)
            {
                CameraMovementStart();
                Debug.Log("movement start");
            }
        }

        if (Input.GetMouseButtonUp(2))
        {
            if(CameraMovementDone != null)
            {
                CameraMovementDone();
                Debug.Log("movement done");
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            OnZoomIn();
        }else if(Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            OnZoomOut();
        }
    }

    public void OnMovementSelected()
    {
        if(MovementSelected != null)
        {
            MovementSelected(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer]);
        }
    }

    public void OnAttackSelected() {
        if (AttackSelected != null)
        {
            AttackSelected(PlayerManager.instance.Players[PlayerManager.instance.ActualPlayer]);
        }
    }

    void OnZoomIn()
    {
        if (ZoomIn != null)
        {
            ZoomIn();
        }
    }

    void OnZoomOut()
    {
        if (ZoomOut != null)
        {
            ZoomOut();
        }
    }

}
