﻿using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class ShopButtonManager : MonoBehaviour {
    public int AmountValue;
    public TextMeshProUGUI Field;
    public TextMeshProUGUI ExperienceField;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnButtonClick);
    }

    public void OnButtonClick()
    {
        if(AmountValue > 0)
        {
            ManageChange(Field, ExperienceField, false);
        }
        else
        {
            ManageChange(ExperienceField, Field, true);
        }
    }

    public void ManageChange(TextMeshProUGUI AddField, TextMeshProUGUI SubtractField, bool NegativeValue)
    {
        if (NegativeValue)
        {
            if (int.Parse(SubtractField.text) + AmountValue >= 0)
            {
                AddField.text = (int.Parse(AddField.text) - AmountValue).ToString();
                SubtractField.text = (int.Parse(SubtractField.text) + AmountValue).ToString();
            }
        }
        else
        {
            if (int.Parse(SubtractField.text) - AmountValue >= 0)
            {
                AddField.text = (int.Parse(AddField.text) + AmountValue).ToString();
                SubtractField.text = (int.Parse(SubtractField.text) - AmountValue).ToString();
            }
        }
    }

}
